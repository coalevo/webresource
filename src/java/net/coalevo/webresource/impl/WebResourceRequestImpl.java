/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.webresource.impl;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.ServiceAgent;
import net.coalevo.foundation.model.UserAgent;
import net.coalevo.webresource.model.WebResourceRequest;
import net.coalevo.webresource.model.WebSession;
import org.apache.commons.codec.binary.Base64InputStream;
import org.apache.commons.codec.binary.Base64OutputStream;

import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * This class implements {@link WebResourceRequest}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class WebResourceRequestImpl
    implements WebResourceRequest {

  private String m_Resource;
  private String m_Query;
  private String[] m_SubResources;
  private HttpServletRequest m_Request;
  private HttpServletResponse m_Response;
  private WebResourceService m_WebResourceService;
  private Map m_RequestParameters;

  public WebResourceRequestImpl(WebResourceService ws, HttpServletRequest req, HttpServletResponse res) {
    m_Request = req;
    m_Response = res;
    m_WebResourceService = ws;
  }//ResourceRequest

  public boolean isGet() {
    return "GET".equals(m_Request.getMethod());
  }//isGet

  public boolean isPut() {
    return "PUT".equals(m_Request.getMethod());
  }//isPut

  public boolean isPost() {
    return "POST".equals(m_Request.getMethod());
  }//isPost

  public boolean isDelete() {
    return "DELETE".equals(m_Request.getMethod());
  }//isDelete

  public String getResource() {
    return m_Resource;
  }//getResource

  public boolean hasSubResources() {
    return m_SubResources != null && m_SubResources.length > 0;
  }//hasSubResource

  public String[] getSubResources() {
    return m_SubResources;
  }//getSubResources

  public boolean hasQuery() {
    return m_Query != null && m_Query.length() > 0;
  }//hasQuery

  public String getQuery() {
    return m_Query;
  }//getQuery

  public Map getParameters() {
    return m_RequestParameters;
  }//getParameters

  public String getParameter(String key) {
    if (m_RequestParameters == null) {
      return null;
    }
    Object o = m_RequestParameters.get(key);
    if (o == null) {
      return null;
    }
    if (o instanceof String[]) {
      String[] params = (String[]) o;
      if (params.length == 0) {
        return null;
      } else {
        return params[0];
      }
    } else {
      return o.toString();
    }
  }//getParameter

  public boolean hasParameter(String key) {
    if (m_RequestParameters == null) {
      return false;
    }
    return m_RequestParameters.containsKey(key);
  }//hasParameter

  public void setSessionAttribute(String key, Object o) {
    m_Request.getSession().setAttribute(key, o);
  }//setSessionAttribute

  public Object getSessionAttribute(String key) {
    return m_Request.getSession().getAttribute(key);
  }//setSessionAttribute

  public void removeSessionAttribute(String key) {
    m_Request.getSession().removeAttribute(key);
  }//removeSessionAttribute

  public boolean hasSessionAttribute(String key) {
    return m_Request.getSession().getAttribute(key) != null;
  }//hasSessionAttribute

  public WebSession getWebSession() {
    return (WebSession) getSessionAttribute(WebResourceConstants.WEBSESSION_ATTRIBUTE_KEY);
  }//getWebSession

  private WebSessionImpl getWebSessionImpl() {
    return (WebSessionImpl) getWebSession();
  }//getWebSessionImpl

  void setWebSession(WebSession ws) {
    setSessionAttribute(WebResourceConstants.WEBSESSION_ATTRIBUTE_KEY, ws);
  }//setWebSession

  public HttpServletRequest getHttpRequest() {
    return m_Request;
  }//getRequest

  public HttpServletResponse getHttpResponse() {
    return m_Response;
  }//getResponse

  public InputStream getInputStream() throws IOException {
    if (isPrivate()) {
      WebSessionImpl ws = getWebSessionImpl();
      InputStream in = m_Request.getInputStream();
      //wrap the stream
      try {
        return new CipherInputStream(
            new Base64InputStream(in),
            ws.getDecryptCipher(getPrivate())
        );
      } catch (Exception ex) {
        return null;
      }
    } else {
      return m_Request.getInputStream();
    }
  }//getInputStream

  public OutputStream getOutputStream() throws IOException {
    if (isAuthenticated() && isPrivate()) {
      WebSessionImpl ws = getWebSessionImpl();
      //set encrypt iv
      String iv = ws.getIVString();
      setPrivate(iv);

      OutputStream out = m_Response.getOutputStream();
      Activator.log().debug("Response committed: " + m_Response.isCommitted());
      try {
        return new CipherOutputStream(
            new MacOutputStream(
                new Base64OutputStream(out), ws.getMac(), m_Response),
            ws.getEncryptCipher(iv)
        );
      } catch (Exception ex) {
        return null;
      }
    } else {
      return m_Response.getOutputStream();
    }
  }//getOutputStream

  public PrintWriter getOutputWriter()
      throws IOException {

    return new PrintWriter(
        new OutputStreamWriter(
            getOutputStream(),
            m_Response.getCharacterEncoding()
        ), false
    );
  }//getOutputWriter

  public void failInternalError() {
    try {
      m_Response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
    } catch (Exception ex) {
      Activator.log().error("ResourceRequest::fail()", ex);
    }
  }//failInternalError

  public void failMethodNotAllowed() {
    try {
      m_Response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
    } catch (Exception ex) {
      Activator.log().error("ResourceRequest::fail()", ex);
    }
  }//failMethodNotAllowed

  public void failBadRequest() {
    try {
      m_Response.sendError(HttpServletResponse.SC_BAD_REQUEST);
    } catch (Exception ex) {
      Activator.log().error("ResourceRequest::fail()", ex);
    }
  }//failBadRequest

  public void failNoContent() {
    try {
      m_Response.sendError(HttpServletResponse.SC_NO_CONTENT);
    } catch (Exception ex) {
      Activator.log().error("ResourceRequest::fail()", ex);
    }
  }//failNoContent

  public void failUnauthorized() {
    try {
      m_Response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
    } catch (Exception ex) {
      Activator.log().error("ResourceRequest::fail()", ex);
    }
  }//failUnauthorized

  public void fail(int code) {
    try {
      m_Response.sendError(code);
    } catch (Exception ex) {
      Activator.log().error("ResourceRequest::fail()", ex);
    }
  }//fail

  public void success() {
    m_Response.setStatus(HttpServletResponse.SC_OK);
  }//success

  public void quickResponse(String str) throws IOException {
    //m_Response.setContentLength(str.length());
    m_Response.setStatus(HttpServletResponse.SC_OK);
    PrintWriter pw = getOutputWriter();
    pw.print(str);
    Activator.log().debug("Response committed (printed something): " + m_Response.isCommitted());
    pw.close();
    Activator.log().debug("Response committed (closed pw): " + m_Response.isCommitted());
  }//quickRespond

  public void setResponseType(String type) {
    m_Response.setContentType(type);
  }//setResponseType

  public void setXMLResponseType() {
    m_Response.setContentType("application/xml");
  }//setXMLResponseType

  public void setJSONResponseType() {
    m_Response.setContentType("application/json");
  }//setJSONResponseType

  public void setImageResponseType(String codec) {
    m_Response.setContentType(String.format("image/%s", codec));
  }//setResponseType

  public boolean redirect(String url) {
    try {
      m_Response.sendRedirect(url);
      return true;
    } catch (Exception ex) {
      Activator.log().error("ResourceRequest::redirect()", ex);
      return false;
    }
  }//redirect

  boolean prepare() {
    Activator.log().debug("PathInfo: " + m_Request.getPathInfo());
    String res = m_Request.getPathInfo().substring(1);
    if (isAuthenticated() && isPrivate()) {
      WebSessionImpl ws = getWebSessionImpl();
      //decrypt request
      String iv = getPrivate();
      try {
        res = ws.decrypt(iv, res, WebResourceConstants.ENCODING_HEX);
      } catch (Exception ex) {
        Activator.log().error("prepare()::" + toString(), ex);
      }
      res = extractQueryParams(res, true);
    } else {
      extractQueryParams(res, false);
    }
    Activator.log().debug("Request after query params: " + m_Request.getPathInfo());
    Activator.log().debug("===> Query :=" + m_Query);
    Activator.log().debug("===> Params:=" + m_RequestParameters);

    //2. extract resource
    final int idx = res.indexOf('/');
    if (idx == -1) {
      m_Resource = res;
    } else {
      m_Resource = res.substring(0, idx);
      if (idx < res.length() - 1) {
        //3. extract subresources
        m_SubResources = res.substring(idx + 1, res.length()).split("/");
      }
    }

/*
      // 1. obtain resource
      final int idx = res.indexOf('/');
      final int pidx = res.indexOf('?');

      if (idx < 0 || idx == res.length() - 1) {
        extractQueryParams()
        m_Resource = res;
      } else {
        // cut resource
        m_Resource = res.substring(0, idx);
      }
      //2. extract query params
      int pidx = extractQueryParams(res);
      if(idx)

      //cut subresources
      m_SubResources = res.substring(idx + 1, pidx).split("/");
      //cut subresources



    } else {
      final int idx = res.indexOf('/');
      if (idx < 0 || idx == res.length() - 1) {
        m_Resource = res;
        m_Query = m_Request.getQueryString();
        m_RequestParameters = m_Request.getParameterMap();
      } else {
        m_Resource = res.substring(0, idx);
        m_SubResources = res.substring(idx + 1, res.length()).split("/");
        m_Query = m_Request.getQueryString();
        m_RequestParameters = m_Request.getParameterMap();
      }
    }
    */
    //Default output encoding should be UTF-8
    m_Response.setCharacterEncoding("UTF-8");
    Activator.log().debug("===> Res   :=" + m_Resource);
    Activator.log().debug("===> SubRes:=" + java.util.Arrays.toString(m_SubResources));
    Activator.log().debug("===> Query :=" + m_Query);
    Activator.log().debug("===> Params:=" + m_RequestParameters);
    return true;
  }//prepare

  private String extractQueryParams(String res, boolean b) {
    //1. check if private
    if (b) {
      // 2. obtain request parameters
      int pidx = res.indexOf("?");
      Activator.log().debug("Queryparam index: " + pidx);
      if (pidx > 0) {
        m_RequestParameters = new HashMap();
        m_Query = res.substring(pidx + 1);
        try {
          RequestUtil.parseParameters(m_RequestParameters, m_Query, "utf-8");
        } catch (UnsupportedEncodingException e) {
        }
        return res.substring(0, pidx);
      }
    } else {
      m_Query = m_Request.getQueryString();
      m_RequestParameters = m_Request.getParameterMap();
    }
    return res;
  }//extractQueryParams

  public String formatUTCDateTime(Date d) {
    return m_WebResourceService.getUTCDateFormatter().format(d);
  }//formatUTCTimeDate

  public String formatUTCDateTime(long d) {
    return m_WebResourceService.getUTCDateFormatter().format(new Date(d));
  }//formatUTCDateTime

  public boolean isAuthenticated() {
    return (
        m_Request.isRequestedSessionIdValid() &&
            hasSessionAttribute(WebResourceConstants.WEBSESSION_ATTRIBUTE_KEY)
    );
  }//isAuthenticated

  public UserAgent getUserAgent() {
    return (UserAgent) getWebSession().getOwner();
  }//getUserAgent

  public void invalidateAuthentication() {
    getWebSession().invalidate();
    removeSessionAttribute(WebResourceConstants.WEBSESSION_ATTRIBUTE_KEY);
  }//invalidateAuthentication

  public String resolveNickname(String aidin) {
    return m_WebResourceService.getNickname(aidin);
  }//resolveNickname

  public AgentIdentifier resolveAID(String nick) {
    return m_WebResourceService.getAgentIdentifier(nick);
  }//resolveAID

  public AgentIdentifier getAgentIdentifier(String aid) {
    return m_WebResourceService.getAgentIdentifier(aid);
  }//getAgentIdentifier

  public ServiceAgent getServiceAgent() {
    return m_WebResourceService.getServiceAgent();
  }//getServiceAgent

  synchronized boolean authenticateTransaction() {
    WebSessionImpl wsi = (WebSessionImpl) getWebSession();
    String ctoken = getAuthenticationToken();
    if (!wsi.isTransaction(ctoken)) {
      return false;
    } else {
      setAuthenticationToken(wsi.getNextToken());
      return true;
    }
  }//authenticateTransaction

  private String getAuthenticationToken() {
    return m_Request.getHeader(WebResourceConstants.AUTHENTICITY_HEADER);
  }//getAuthenticationToken

  private void setAuthenticationToken(String token) {
    m_Response.setHeader(WebResourceConstants.AUTHENTICITY_HEADER, token);
  }//setAuthenticationToken

  private boolean isPrivate() {
    return getPrivate() != null;
  }//isPrivate

  private String getPrivate() {
    return m_Request.getHeader(WebResourceConstants.CONFIDENTIALITY_HEADER);
  }//getPrivate

  private void setPrivate(String iv) {
    m_Response.setHeader(WebResourceConstants.CONFIDENTIALITY_HEADER, iv);
  }//setPrivate

  public String toString() {
    StringBuilder sbuf = new StringBuilder(super.toString());
    sbuf.append("{");
    sbuf.append(m_Resource);
    sbuf.append(",");
    sbuf.append(Arrays.toString(m_SubResources));
    sbuf.append(",");
    sbuf.append(m_Request.toString());
    sbuf.append(",");
    sbuf.append(m_Response.toString());
    sbuf.append("}");
    return sbuf.toString();
  }//toString

}//class WebResourceRequestImpl
