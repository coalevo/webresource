/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.webresource.impl;

import net.coalevo.foundation.model.Session;
import net.coalevo.foundation.model.UserAgent;
import net.coalevo.foundation.util.crypto.KDF;
import net.coalevo.foundation.util.crypto.MacFactory;
import net.coalevo.foundation.util.crypto.OneTimeTokenGenerator;
import net.coalevo.foundation.util.crypto.RandomUtil;
import net.coalevo.foundation.util.srp.ServerSRP;
import net.coalevo.security.service.SecurityService;
import net.coalevo.system.service.SessionService;
import net.coalevo.webresource.model.*;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import javax.crypto.Mac;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Implements a {@link WebResource} for session handling.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class SessionWebResource
    extends BaseWebResource {

  private static final Marker c_LogMarker = MarkerFactory.getMarker(SessionWebResource.class.getName());

  public SessionWebResource() {
    super(URI_NAME);
  }//constructor

  public void handle(WebResourceRequest req)
      throws WebResourceException {
    Activator.log().debug(req.toString());
    if (req.isGet()) {
      if (!req.isAuthenticated()) {
        if (req.hasSubResources()) {
          req.failBadRequest();
          return;
        }
        if (req.hasParameter(PARAM_AID)
            && req.hasParameter(PARAM_A)
            && !hasSRP(req)) {
          doInit(req);
          return;
        }
        if (req.hasParameter(PARAM_M)
            && hasSRP(req)) {
          doMatch(req);
          return;
        }
        removeSRP(req);
        req.failBadRequest();
      } else {
        String[] subres = req.getSubResources();
        if (!req.hasSubResources() || subres.length != 1) {
          req.failBadRequest();
          return;
        }
        String sub = subres[0];
        if (SUB_LOGOUT.equals(sub)) {
          req.invalidateAuthentication();
          req.success();
          return;
        } else if (SUB_INFO.equals(sub)) {
          doInfo(req);
          return;
        } else {
          req.failBadRequest();
          return;
        }
      }

    } else {
      req.failMethodNotAllowed();
    }
  }//handle

  /**
   * Client sent public ephemeral and identifier.
   *
   * @param req the corresponding {@link WebResourceRequest}.
   */
  private void doInit(WebResourceRequest req) {
    //retrieve parameters
    String aid = req.getParameter(PARAM_AID);
    String A = req.getParameter(PARAM_A);

    //retrieve server side SRP object
    SecurityService sec =
        Activator.getServices().getSecurityService(ServiceMediator.NO_WAIT);
    if (sec == null) {
      req.failInternalError();
      return;
    }
    ServerSRP sSRP = sec.getServerSRP(aid.toString());

    sSRP.received(A);
    //store in session
    setSRP(req, sSRP);
    //Server respond with salt and ephemeral
    try {
      if (req.hasParameter(PARAM_FORMAT)
          && "JSON".equalsIgnoreCase(req.getParameter(PARAM_FORMAT))) {
        req.setJSONResponseType();
        req.quickResponse(String.format(
            INIT_RES_JSON,
            toJSONString(sSRP.getSalt(), false),
            toJSONString(sSRP.getPublicEphemeral(), false)
        ));
      } else {
        req.setXMLResponseType();
        req.quickResponse(String.format(
            INIT_RES_XML,
            sSRP.getSalt(),
            sSRP.getPublicEphemeral()
        ));
      }
    } catch (IOException ex) {
      removeSRP(req);
      //TODO: Log
    }

  }//doInit

  public void doMatch(WebResourceRequest req) {
    WebResourceRequestImpl reqi = (WebResourceRequestImpl) req;
    if (!checkTimestamp(req)) {
      removeSRP(req);
      req.failUnauthorized();
      return;
    }
    ServerSRP sSRP = getSRP(req);
    removeSRP(req);

    String M = req.getParameter(PARAM_M);
    if (sSRP.checkClientKeyProof(M)) {
      // Init session
      SecurityService sec =
          Activator.getServices().getSecurityService(ServiceMediator.NO_WAIT);
      SessionService ses =
          Activator.getServices().getSessionService(ServiceMediator.NO_WAIT);
      if (sec == null) {
        req.failInternalError();
        return;
      }
      Session s = ses.initiateSession(sec.getUserAgent(sSRP), 2 * 60 * 60, TimeUnit.SECONDS);
      WebSessionImpl wsi = new WebSessionImpl(s);

      // Prepare KDF and OTT seeds
      byte ott = RandomUtil.nextByte();
      byte kdf = RandomUtil.nextByte();
      byte ity = RandomUtil.nextByte();

      // Prepare KDF & OTT
      KDF kdfu = new KDF();
      OneTimeTokenGenerator ottgen = new OneTimeTokenGenerator();

      try {
        //Initialize key derivation function (KDF)
        kdfu.init(sSRP.getSessionKey(), kdf);
        //Derive session key
        byte[] seskey = kdfu.nextKey(128);
        //Initialize one time token generator (OTT)
        ottgen.init(seskey, ott);
        //Initialize Mac
        KDF kdfi = new KDF();
        kdfi.init(sSRP.getSessionKey(), ity);
        Mac m = MacFactory.createMac(kdfi.nextKey(128));
        //Initialize web session
        wsi.prepareAuthenticated(seskey, kdfu, ottgen, m);
        //Store websession
        reqi.setWebSession(wsi);
        //remove SRP
        removeSRP(req);

      } catch (Exception ex) {
        Activator.log().error(c_LogMarker, "doMatch()", ex);
        req.failInternalError();
        return;
      }

      try {
        if (req.hasParameter(PARAM_FORMAT)
            && "JSON".equalsIgnoreCase(req.getParameter(PARAM_FORMAT))) {
          req.setJSONResponseType();
          req.quickResponse(String.format(
              MATCH_RES_JSON,
              toJSONString(sSRP.getKeyMatchProof(), false),
              (kdf & 0xFF),
              (ott & 0xFF),
              (ity & 0xFF)
          ));
        } else {
          req.setXMLResponseType();
          req.quickResponse(String.format(
              MATCH_RES_XML,
              sSRP.getKeyMatchProof(),
              (kdf & 0xFF),
              (ott & 0xFF),
              (ity & 0xFF)
          ));
        }
      } catch (IOException ex) {
        //TODO: Log
      }
    } else {
      removeSRP(req);
      req.failUnauthorized();
    }
  }//doMatch

  public void doInfo(WebResourceRequest req) {
    try {
      WebSession ws = req.getWebSession();
      UserAgent ag = req.getUserAgent();
      Session ses = ag.getSession();
      if (req.hasParameter(PARAM_FORMAT)
          && "JSON".equalsIgnoreCase(req.getParameter(PARAM_FORMAT))) {
        req.setJSONResponseType();
        req.quickResponse(String.format(
            INFO_RES_JSON,
            toJSONString(ag.getIdentifier(), false),
            toJSONString((ses == null) ? "no session" : ws.getCoalevoSession().getIdentifier(), false)
        ));
      } else {
        req.setXMLResponseType();
        req.quickResponse(String.format(
            INFO_RES_XML,
            ses.getAgent().toString(),
            ses.getIdentifier()
        ));
      }
    } catch (IOException ex) {
      //TODO: Log
    }
  }//doInfo

  private void setSRP(WebResourceRequest req, ServerSRP srp) {
    req.setSessionAttribute(ATTR_SRP, srp);
    req.setSessionAttribute(ATTR_INITTS, new Long(System.currentTimeMillis()));
  }//setSRP

  private ServerSRP getSRP(WebResourceRequest req) {
    return (ServerSRP) req.getSessionAttribute(ATTR_SRP);
  }//getSRP

  private void removeSRP(WebResourceRequest req) {
    req.removeSessionAttribute(ATTR_SRP);
    req.removeSessionAttribute(ATTR_INITTS);
  }//removeSRP

  private boolean hasSRP(WebResourceRequest req) {
    return req.hasSessionAttribute(ATTR_SRP);
  }//hasSRP

  private boolean checkTimestamp(WebResourceRequest req) {
    try {
      Long l = (Long) req.getSessionAttribute(ATTR_INITTS);
      return System.currentTimeMillis() - l.longValue() < AUTH_TIMEOUT;
    } finally {
      req.removeSessionAttribute(ATTR_INITTS);
    }
  }//checkTimestamp

  private static final String URI_NAME = "session";
  private static final String PARAM_A = "a";
  private static final String PARAM_M = "m";
  private static final String PARAM_AID = "aid";
  private static final String PARAM_FORMAT = "format";
  private static final String PARAM_OTT = "ott";

  private static final String SUB_LOGOUT = "logout";
  private static final String SUB_INFO = "info";

  private static final String ATTR_SRP = URI_NAME + ".srp";
  private static final String ATTR_INITTS = URI_NAME + ".its";

  private static final String INIT_RES_JSON = "{\"salt\":\"%1$s\",\"b\":\"%2$s\"}";
  private static final String INIT_RES_XML = "<session salt=\"%1$s\" b=\"%2$s\" />";

  private static final String MATCH_RES_JSON = "{\"m\":\"%1$s\",\"kd\":\"%2$d\",\"ott\":\"%3$d\",\"ity\":\"%4$d\"}";
  private static final String MATCH_RES_XML = "<session m=\"%1$s\" kd=\"%2$d\" ott=\"%3$d\" ity=\"%4$d\"/>";

  private static final String INFO_RES_JSON = "{\"aid\":\"%1$s\",\"sid\":\"%2$s\"}";
  private static final String INFO_RES_XML = "<session aid=\"%1$s\" sid=\"%2$s\" />";

  private static long AUTH_TIMEOUT = 1000 * 60 * 5;

}//class SessionWebResource
