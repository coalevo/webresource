/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.webresource.impl;

import net.coalevo.foundation.model.Agent;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.Session;
import net.coalevo.foundation.util.crypto.*;
import net.coalevo.webresource.model.WebSession;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import javax.crypto.Cipher;
import javax.crypto.Mac;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Base class implementing an extensible basic {@link WebSession}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class WebSessionImpl
    implements WebSession {

  private static Marker c_LogMarker = MarkerFactory.getMarker(WebSessionImpl.class.getName());
  protected AtomicBoolean m_Valid;
  protected Session m_CoalevoSession;
  protected OneTimeTokenGenerator m_OTT;
  protected KDF m_KDF;
  protected Mac m_Mac;
  
  //Encryption
  protected BlockCipherFactory m_CipherFactory;
  private byte[] m_SessionCryptKey;

  public WebSessionImpl(Session s) {
    m_Valid = new AtomicBoolean(true);
    m_CoalevoSession = s;
  }//constructor

  public AgentIdentifier getOwnerIdentifier() {
    return m_CoalevoSession.getAgent().getAgentIdentifier();
  }//getOwnerIdentifier

  public Agent getOwner() {
    return m_CoalevoSession.getAgent();
  }//getOwner

  public Session getCoalevoSession() {
    return m_CoalevoSession;
  }///getCoalevoSession

  void prepareAuthenticated(byte[] seskey, KDF kdf, OneTimeTokenGenerator ott, Mac mac)
      throws Exception {
    m_SessionCryptKey = seskey;
    m_OTT = ott;
    m_KDF = kdf;
    m_Mac = mac;
    m_CipherFactory = new BlockCipherFactory();
  }//prepareEncryption
  
  public void invalidate() {
    if (m_Valid.getAndSet(false)) {
      Activator.log().debug(c_LogMarker, "invalidate()::Unregistering presence.");
      if (m_CoalevoSession != null) {
        m_CoalevoSession.invalidate();
      }
      //null out session ref.
      m_CoalevoSession = null;
    }
  }//invalidate

  public boolean isValid() {
    return m_Valid.get();
  }//isValid

  public boolean isAuthenticated() {
    return m_CoalevoSession != null && m_CoalevoSession.isValid();
  }//isAuthenticated

  public void activity() {
    if (m_CoalevoSession != null) {
      m_CoalevoSession.access();
    }
  }//activity

  public Cipher getDecryptCipher(byte[] iv) throws Exception {
    return m_CipherFactory.createDecryptionCypher(
        m_SessionCryptKey,
        iv);
  }//getDecryptCipher

  public Cipher getDecryptCipher(String iv) throws Exception {
    return m_CipherFactory.createDecryptionCypher(
        m_SessionCryptKey,
        decodeIV(iv));
  }//getDecryptCipher

  public Cipher getEncryptCipher(byte[] iv) throws Exception {
    return m_CipherFactory.createEncryptionCypher(m_SessionCryptKey, iv);
  }//getEncryptCipher

  public Cipher getEncryptCipher(String iv) throws Exception {
    return m_CipherFactory.createEncryptionCypher(m_SessionCryptKey,
        decodeIV(iv));
  }//getDecryptCipher

  public String getIVString() {
    try {
      byte[] iv = RandomUtil.getBlock(16);
      return new String(Base64.encodeBase64(iv), "utf-8");
    } catch (Exception ex) {
      //should not happen
    }
    return "";
  }//getIVString

  public byte[] decodeIV(String str) {
    try {
      return Base64.decodeBase64(str.getBytes("utf-8"));
    } catch (Exception ex) {
      //should not happen
    }
    return null;
  }//decodeIV

  public String decrypt(String ivstr, String str, String encoding)
      throws IOException {
    try {
      //1. Decode IV
      byte[] iv = Base64.decodeBase64(ivstr.getBytes("utf-8"));
      byte[] ct;
      if (WebResourceConstants.ENCODING_HEX.equals(encoding)) {
        ct = Hex.decodeHex(str.toCharArray());
      } else {
        ct = Base64.decodeBase64(str.getBytes("utf-8"));
      }
      //3. Get Cipher with IV
      Cipher c = getDecryptCipher(iv);

      //4. Decrypt
      byte[] pt = c.doFinal(ct);

      //5. Return as String
      return new String(pt, "utf-8");
    } catch (Exception ex) {
      Activator.log().error(c_LogMarker, "decrypt()", ex);
      return "";
    }
  }//decrypt

  public String encrypt(String ivstr, String str, String encoding)
      throws IOException {
    try {
      //1. IV
      byte[] iv = Base64.decodeBase64(ivstr.getBytes("utf-8"));
      //2. Get Cipher
      Cipher c = getEncryptCipher(iv);
      //3. Encrypt
      byte[] ct = c.doFinal(str.getBytes("utf-8"));
      //4. Encode
      if (WebResourceConstants.ENCODING_HEX.equals(encoding)) {
        return new String(Hex.encodeHex(ct));
      } else {
        return new String(Base64.encodeBase64(ct));
      }
    } catch (Exception ex) {
      Activator.log().error(c_LogMarker, "encrypt()", ex);
      return "";
    }
  }//encrypt

  public Mac getMac() {
    return m_Mac;
  }//getMac

  boolean isTransaction(String token)
      throws SecurityException {

    if (token == null || token.length() == 0) {
      throw new SecurityException();
    }

    return token.equals(getNextToken());
  }//isTransaction

  String getNextToken() {
    return new String(Base64.encodeBase64(m_OTT.nextTokenRaw()));
  }//getToken

}//class WebSessionImpl
