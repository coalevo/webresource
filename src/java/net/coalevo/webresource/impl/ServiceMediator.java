/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.webresource.impl;

import net.coalevo.foundation.service.MessageResourceService;
import net.coalevo.foundation.util.ConfigurationMediator;
import net.coalevo.security.service.SecurityService;
import net.coalevo.statistics.service.StatisticsService;
import net.coalevo.system.service.SessionService;
import net.coalevo.userdata.service.UserdataService;
//import org.ops4j.pax.web.service.WebContainer;
import org.osgi.framework.*;
import org.osgi.service.http.HttpService;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;
import java.util.Vector;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Provides a mediator for required coalevo services.
 * Allows to obtain fresh and latest references by using
 * the whiteboard model to track the services at all times.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class ServiceMediator {

  private Marker m_LogMarker = MarkerFactory.getMarker(ServiceMediator.class.getName());
  private BundleContext m_BundleContext;

  private SecurityService m_SecurityService;
  private MessageResourceService m_MessageResourceService;
  private StatisticsService m_StatisticsService;
  private SessionService m_SessionService;
  private HttpService m_HttpService;
  private UserdataService m_UserdataService;
  //private WebContainer m_WebContainer;
  
  private CountDownLatch m_SecurityServiceLatch;
  private CountDownLatch m_MessageResourceServiceLatch;
  private CountDownLatch m_StatisticsServiceLatch;
  private CountDownLatch m_SessionServiceLatch;
  private CountDownLatch m_HttpServiceLatch;
  private CountDownLatch m_UserdataServiceLatch;
 // private CountDownLatch m_WebContainerLatch;

  private ConfigurationMediator m_ConfigurationMediator;

  private String m_ServerAddress;
  private String m_ServerPort;
  private String m_WSContext;

  public ServiceMediator() {

  }//constructor

  public SecurityService getSecurityService(long wait) {
    try {
      if (wait < 0) {
        m_SecurityServiceLatch.await();
      } else if (wait > 0) {
        m_SecurityServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      Activator.log().error(m_LogMarker, "getSecurityService()", e);
    }
    return m_SecurityService;
  }//getSecurityService

  public MessageResourceService getMessageResourceService(long wait) {
    try {
      if (wait < 0) {
        m_MessageResourceServiceLatch.await();
      } else if (wait > 0) {
        m_MessageResourceServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      Activator.log().error(m_LogMarker, "getMessageResourceService()", e);
    }
    return m_MessageResourceService;
  }//getMessageResourceService

  public SessionService getSessionService(long wait) {
    try {
      if (wait < 0) {
        m_SessionServiceLatch.await();
      } else if (wait > 0) {
        m_SessionServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      Activator.log().error(m_LogMarker, "getSessionService()", e);
    }

    return m_SessionService;
  }//getSessionService

  public StatisticsService getStatisticsService(long wait) {
    try {
      if (wait < 0) {
        m_StatisticsServiceLatch.await();
      } else if (wait > 0) {
        m_StatisticsServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      Activator.log().error(m_LogMarker, "getStatisticsService()", e);
    }
    return m_StatisticsService;
  }//getStatisticsService

  public HttpService getHttpService(long wait) {
    try {
      if (wait < 0) {
        m_HttpServiceLatch.await();
      } else if (wait > 0) {
        m_HttpServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      Activator.log().error(m_LogMarker, "getHttpService()", e);

    }
    return m_HttpService;
  }//getHttpService

/*
  public WebContainer getWebContainer(long wait) {
    try {
      if (wait < 0) {
        m_WebContainerLatch.await();
      } else if (wait > 0) {
        m_WebContainerLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      Activator.log().error(m_LogMarker, "getWebContainer()", e);
    }
    return m_WebContainer;
  }//getWebContainer
*/

  public UserdataService getUserdataService(long wait) {
    try {
      if (wait < 0) {
        m_UserdataServiceLatch.await();
      } else if (wait > 0) {
        m_UserdataServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      Activator.log().error(m_LogMarker, "getUserdataService()", e);
    }

    return m_UserdataService;
  }//getUserdataService

  public ConfigurationMediator getConfigMediator() {
    return m_ConfigurationMediator;
  }//getConfigMediator

  public void setConfigMediator(ConfigurationMediator configMediator) {
    m_ConfigurationMediator = configMediator;
  }//setConfigMediator

  public String getServerPort() {
    return m_ServerPort;
  }//setServerPort

  public String getServerAddress() {
    return m_ServerAddress;
  }//getServerAddress

  public String getWSContext() {
    return m_WSContext;
  }//getWSContext

  public void setWSContext(String WSContext) {
    m_WSContext = WSContext;
  }//setWSContext

  public boolean activate(BundleContext bc) {
    //get the context
    m_BundleContext = bc;
    //prepare waits if required
    m_SecurityServiceLatch = createWaitLatch();
    m_MessageResourceServiceLatch = createWaitLatch();
    m_SessionServiceLatch = createWaitLatch();
    m_StatisticsServiceLatch = createWaitLatch();
    m_HttpServiceLatch = createWaitLatch();
    m_UserdataServiceLatch = createWaitLatch();
    //m_WebContainerLatch = createWaitLatch();

    //prepareDefinitions listener
    ServiceListener serviceListener = new ServiceListenerImpl();

    //prepareDefinitions the filter
    String filter =
        "(|(|(|(|(|(objectclass=" + SecurityService.class.getName() + ")" +
            "(objectclass=" + MessageResourceService.class.getName() + "))" +
            "(objectclass=" + SessionService.class.getName() + "))" +
            "(objectclass=" + StatisticsService.class.getName() + "))" +
            "(objectclass=" + HttpService.class.getName() + "))" +
            "(objectclass=" + UserdataService.class.getName() + "))"; // +
    //        "(objectclass=" + WebContainer.class.getName() + "))";

    try {
      //add the listener to the bundle context.
      bc.addServiceListener(serviceListener, filter);

      //ensure that already registered Service instances are registered with
      //the manager
      ServiceReference[] srl = bc.getServiceReferences(null, filter);
      for (int i = 0; srl != null && i < srl.length; i++) {
        serviceListener.serviceChanged(new ServiceEvent(ServiceEvent.REGISTERED, srl[i]));
      }
    } catch (InvalidSyntaxException ex) {
      Activator.log().error(m_LogMarker, "activate()", ex);
      return false;
    }
    return true;
  }//activate

  public void deactivate() {
    //null out the references
    m_SecurityService = null;
    m_MessageResourceService = null;
    m_SessionService = null;
    m_ConfigurationMediator = null;
    m_StatisticsService = null;
    m_HttpService = null;
    m_UserdataService = null;
    //m_WebContainer = null;

    //Latches
    if (m_SecurityServiceLatch != null) {
      m_SecurityServiceLatch.countDown();
      m_SecurityServiceLatch = null;
    }
    if (m_MessageResourceServiceLatch != null) {
      m_MessageResourceServiceLatch.countDown();
      m_MessageResourceServiceLatch = null;
    }
    if (m_SessionServiceLatch != null) {
      m_SessionServiceLatch.countDown();
      m_SessionServiceLatch = null;
    }
    if (m_StatisticsServiceLatch != null) {
      m_StatisticsServiceLatch.countDown();
      m_StatisticsServiceLatch = null;
    }
    if (m_HttpServiceLatch != null) {
      m_HttpServiceLatch.countDown();
      m_HttpServiceLatch = null;
    }
    if (m_UserdataServiceLatch != null) {
      m_UserdataServiceLatch.countDown();
      m_UserdataServiceLatch = null;
    }
   /* if (m_WebContainerLatch != null) {
      m_WebContainerLatch.countDown();
      m_WebContainerLatch = null;
    }
*/
    m_LogMarker = null;
    m_BundleContext = null;
  }//deactivate

  private CountDownLatch createWaitLatch() {
    return new CountDownLatch(1);
  }//createWaitLatch

  private class ServiceListenerImpl
      implements ServiceListener {

    public void serviceChanged(ServiceEvent ev) {
      //System.err.println(ev.toString());
      ServiceReference sr = ev.getServiceReference();
      Object o = null;
      switch (ev.getType()) {
        case ServiceEvent.REGISTERED:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            return;
          } else if (o instanceof SecurityService) {
            m_SecurityService = (SecurityService) o;
            m_SecurityServiceLatch.countDown();
          } else if (o instanceof MessageResourceService) {
            m_MessageResourceService = (MessageResourceService) o;
            m_MessageResourceServiceLatch.countDown();
          } else if (o instanceof SessionService) {
            m_SessionService = (SessionService) o;
            m_SessionServiceLatch.countDown();
          } else if (o instanceof StatisticsService) {
            m_StatisticsService = (StatisticsService) o;
            m_StatisticsServiceLatch.countDown();
          } /*else if (o instanceof WebContainer) {
            m_WebContainer = (WebContainer) o;
            m_WebContainerLatch.countDown();
            //obtain properties
            Object o2 = sr.getProperty("org.ops4j.pax.web.listening.addresses");
            if (o2 == null || o2.toString().length() == 0) {
              //grep local address
              try {
                m_ServerAddress = getLocalPrivateAddresses('c')[0].getHostAddress();
              } catch (Exception ex) {

              }
            } else {
              m_ServerAddress = o2.toString();
            }
            o2 = sr.getProperty("org.osgi.service.http.port");
            if (o2 == null || o2.toString().length() == 0) {
              m_ServerPort = "80";
            } else {
              m_ServerPort = o2.toString();
            }
          }*/ else if (o instanceof HttpService) {
            m_HttpService = (HttpService) o;
            m_HttpServiceLatch.countDown();
          } else if (o instanceof UserdataService) {
            m_UserdataService = (UserdataService) o;
            m_UserdataServiceLatch.countDown();
          } else {
            m_BundleContext.ungetService(sr);
          }

          break;
        case ServiceEvent.UNREGISTERING:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            return;
          } else if (o instanceof SecurityService) {
            m_SecurityService = null;
            m_SecurityServiceLatch = createWaitLatch();
          } else if (o instanceof MessageResourceService) {
            m_MessageResourceService = null;
            m_MessageResourceServiceLatch = createWaitLatch();
          } else if (o instanceof SessionService) {
            m_SessionService = null;
            m_SessionServiceLatch = createWaitLatch();
          } else if (o instanceof StatisticsService) {
            m_StatisticsService = null;
            m_StatisticsServiceLatch = createWaitLatch();
          }/* else if (o instanceof WebContainer) {
            m_WebContainer = null;
            m_WebContainerLatch = createWaitLatch();
          } */else if (o instanceof HttpService) {
            m_HttpService = null;
            m_HttpServiceLatch = createWaitLatch();
          } else if (o instanceof UserdataService) {
            m_UserdataService = null;
            m_UserdataServiceLatch = createWaitLatch();
          } else {
            m_BundleContext.ungetService(sr);
          }
          break;
      }
    }
  }//inner class ServiceListenerImpl

  private static InetAddress[] getLocalPrivateAddresses(char cl)
      throws Exception {
    Vector v = new Vector();
    for (Enumeration e = NetworkInterface.getNetworkInterfaces(); e.hasMoreElements();) {
      NetworkInterface ni = (NetworkInterface) e.nextElement();
      for (Enumeration e2 = ni.getInetAddresses(); e2.hasMoreElements();) {
        InetAddress ia = (InetAddress) e2.nextElement();
        switch (cl) {
          case 'a':
            if (ia.getHostAddress().startsWith("10.")) {
              v.addElement(ia);
            }
            break;
          case 'b':
            if (ia.getHostAddress().startsWith("172.")) {
              v.addElement(ia);
            }
            break;
          case 'c':
            if (ia.getHostAddress().startsWith("192.168.")) {
              v.addElement(ia);
            }
            break;
          default:
            v.addElement(ia);
            break;
        }
      }
    }
    InetAddress[] addrs = new InetAddress[v.size()];
    for (int i = 0; i < addrs.length; i++) {
      addrs[i] = (InetAddress) v.elementAt(i);
    }
    return addrs;
  }//getLocalPrivateAddresses

  public static long WAIT_UNLIMITED = -1;
  public static long NO_WAIT = 0;

}//class ServiceMediator
