/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.webresource.impl;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.Mac;
import javax.servlet.http.HttpServletResponse;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

class MacOutputStream extends FilterOutputStream {

  protected Mac m_Mac;
  protected HttpServletResponse m_Response;

  public MacOutputStream(OutputStream out, Mac mac, HttpServletResponse hsp) {
    super(out);
    m_Mac = mac;
    m_Response = hsp;
  }//constructor

  public void write(int b) throws IOException {
    m_Mac.update((byte) b);
    out.write(b);
  }//write

  public void write(byte[] b, int off, int len)
      throws IOException {

    m_Mac.update(b, off, len);
    out.write(b, off, len);
  }//write

  public void flush() throws IOException {
    Activator.log().debug("Flush was called: " + m_Response.isCommitted());
   // no out.flush();
  }//flush

  public void close() throws IOException {
    try {
      Activator.log().debug("Closing MacOutputStream: committed: " + m_Response.isCommitted());
      //Add Mac
      byte[] code = m_Mac.doFinal();
      m_Response.setHeader(WebResourceConstants.INTEGRITY_HEADER,
          new String(Base64.encodeBase64String(code)));
      m_Mac.reset();
      Activator.log().debug("Header set");

    } catch (Exception ex) {
    }
    try {
      out.flush();
    } catch (IOException ignored) {
    }


    out.close();
  }//close

}//MacOutputStream
