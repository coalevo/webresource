/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.webresource.impl;

import net.coalevo.webresource.model.WebResource;
import org.osgi.framework.*;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * This is the manager for {@link WebResource} instances, which
 * are handled according to the OSGi white-board model.
 * All registered instances of the {@link WebResource} class will be
 * handled here to make them available.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class WebResourceManager {

  private Marker m_LogMarker = MarkerFactory.getMarker(WebResourceManager.class.getName());
  private BundleContext m_BundleContext;
  private Map<String, WebResource> m_WebResources;
  private WebResourceListener m_Listener;

  public WebResourceManager() {
  }//constructor

  public void activate(BundleContext bc) {
    //get the context
    m_BundleContext = bc;

    m_WebResources = new ConcurrentHashMap<String, WebResource>();
    //prepare listener
    m_Listener = new WebResourceListener();

    //prepare the filter
    String filter = "(objectclass=" + WebResource.class.getName() + ")";

    try {
      //add the listener to the bundle context.
      bc.addServiceListener(m_Listener, filter);

      //ensure that already registered Provider instances are registered with
      //the manager
      ServiceReference[] srl = bc.getServiceReferences(null, filter);
      for (int i = 0; srl != null && i < srl.length; i++) {
        m_Listener.serviceChanged(new ServiceEvent(ServiceEvent.REGISTERED, srl[i]));
      }
    } catch (InvalidSyntaxException ex) {
      Activator.log().error(m_LogMarker, "activate()", ex);
    }
  }//activate

  public void deactivate() {
    m_BundleContext.removeServiceListener(m_Listener);
    m_WebResources.clear();

    m_BundleContext = null;
    m_WebResources = null;
  }//deactivate

  public WebResource getWebResource(String id) {
    return m_WebResources.get(id);
  }//getWebResource

  public void register(WebResource wr) {
    final String id = wr.getName();

    //Set binding info
    ServiceMediator sm = Activator.getServices();
    wr.setResourceBinding(sm.getServerAddress(), sm.getServerPort(), sm.getWSContext());

    m_WebResources.put(id, wr);
    Activator.log().info(m_LogMarker, "Registered webresource resource " + id + ".");
  }//register

  public void unregister(WebResource wr) {
    final String id = wr.getName();
    m_WebResources.remove(id);
    Activator.log().info(m_LogMarker, "Unregistered WebResource " + id);
  }//unregister

  private class WebResourceListener
      implements ServiceListener {

    private Marker m_LogMarker = MarkerFactory.getMarker(WebResourceListener.class.getName());

    public void serviceChanged(ServiceEvent ev) {
      ServiceReference sr = ev.getServiceReference();
      Object o = null;
      switch (ev.getType()) {
        case ServiceEvent.REGISTERED:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            Activator.log().error(m_LogMarker, "ServiceListener:serviceChanged:registered:null");
          } else if (!(o instanceof WebResource)) {
            Activator.log().error(m_LogMarker, "ServiceListener:serviceChanged:registered:Reference not a WebResource instance.");
          } else {
            register((WebResource) o);
          }
          break;
        case ServiceEvent.UNREGISTERING:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            Activator.log().error(m_LogMarker, "ServiceListener:serviceChanged:unregistering:null");
          } else if (!(o instanceof WebResource)) {
            Activator.log().error(m_LogMarker, "ServiceListener:serviceChanged:unregistering:Reference not a WebResource instance.");
          } else {
            unregister((WebResource) o);
          }
          break;
      }
    }
  }//inner class WebResourceListener

}//class WebResourceManager
