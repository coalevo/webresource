/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.webresource.impl;

import net.coalevo.foundation.model.*;
import net.coalevo.foundation.util.ConfigurationUpdateHandler;
import net.coalevo.foundation.util.LRUCacheMap;
import net.coalevo.foundation.util.metatype.MetaTypeDictionary;
import net.coalevo.foundation.util.metatype.MetaTypeDictionaryException;
import net.coalevo.security.service.SecurityService;
import net.coalevo.text.util.DateFormatter;
import net.coalevo.userdata.service.UserdataService;
import net.coalevo.webresource.Configuration;
import net.coalevo.webresource.model.WebResource;
import net.coalevo.webresource.model.WebResourceException;
//import org.mortbay.servlet.MultiPartFilter;
//import org.ops4j.pax.web.service.WebContainer;
import org.osgi.service.http.HttpService;
import org.osgi.framework.BundleContext;
import org.osgi.service.http.HttpContext;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Dictionary;
import java.util.NoSuchElementException;
import java.util.Properties;

/**
 * This class implements the WebResourceService.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class WebResourceService
    extends BaseService
    implements Maintainable, ConfigurationUpdateHandler {

  private Marker m_LogMarker = MarkerFactory.getMarker(WebResourceService.class.getName());
  private WebResourceManager m_WebResourceManager;
  private AgentIdentifierInstanceCache m_AIDCache;
  private DateFormatter m_UTCDateFormatter;
  private LRUCacheMap<String, String> m_NickCache;
  private ServiceAgent m_ServiceAgent;
  private HttpServlet m_WSServlet;
  private MainResourceContext m_MainResourceContext;

 // private MultiPartFilter m_MPFilter;

  public WebResourceService() {
    super(WebResourceService.class.getName(),
        new Action[]{Maintainable.DO_MAINTENANCE});
  }//constructor

  public boolean activate(BundleContext bc) {
    ServiceMediator sm = Activator.getServices();
    SecurityService secs =
        sm.getSecurityService(ServiceMediator.WAIT_UNLIMITED);
    m_ServiceAgent = secs.authenticate(this);

    //Prepare WebResourceManager
    m_WebResourceManager = new WebResourceManager();
    m_WebResourceManager.activate(bc);

    //Caches
    int aidcs = 100;
    int nickcs = 100;
    int utcdfpool = 10;
    MetaTypeDictionary mtd = sm.getConfigMediator().getConfiguration();
    try {
      aidcs = mtd.getInteger(Configuration.CACHE_AGENTIDENTIFIERS_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          Activator.getBundleMessages().get("WebResourceService.activation.configexception", "attribute", Configuration.CACHE_AGENTIDENTIFIERS_KEY),
          ex
      );
    }
    try {
      nickcs = mtd.getInteger(Configuration.CACHE_NICKS_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          Activator.getBundleMessages().get("WebResourceService.activation.configexception", "attribute", Configuration.CACHE_NICKS_KEY),
          ex
      );
    }
    try {
      utcdfpool = mtd.getInteger(Configuration.POOLSIZE_UTCDATEFORMATTER_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          Activator.getBundleMessages().get("WebResourceService.activation.configexception", "attribute", Configuration.POOLSIZE_UTCDATEFORMATTER_KEY),
          ex
      );
    }

    m_AIDCache = new AgentIdentifierInstanceCache(aidcs);
    m_NickCache = new LRUCacheMap<String, String>(nickcs);
    m_UTCDateFormatter = new DateFormatter(UTC_STD_PATTERN, utcdfpool);

    try {
      //Activator.log().debug(m_LogMarker, "activate()::waiting for webcontainer.");
      Activator.log().debug(m_LogMarker, "activate()::waiting for httpservice.");
      //WebContainer wc = sm.getWebContainer(ServiceMediator.WAIT_UNLIMITED);
      //HttpContext ctx = wc.createDefaultHttpContext();
      HttpService hs = sm.getHttpService(ServiceMediator.WAIT_UNLIMITED);
      HttpContext hc = hs.createDefaultHttpContext();
     
      //Activator.log().debug(m_LogMarker, "activate()::got webcontainer.");
      Activator.log().debug(m_LogMarker, "activate()::got httpservice.");
      
      //Register Main Resource handler
      m_MainResourceContext = new MainResourceContext();
      hs.registerResources("/","/www/", m_MainResourceContext);
      Activator.log().debug(m_LogMarker, "activate()::registered main resource servlet.");

      //Register web resource context
      String[] uris = {sm.getWSContext() + "*"};
      m_WSServlet = new WebResourceServlet();
      hs.registerServlet(
          sm.getWSContext(),
          m_WSServlet,
          null,
          hc
      );
      Activator.log().debug(m_LogMarker, "activate()::registered ws servlet.");

      /*
      //Register Filter
      Dictionary init = new Properties();
      init.put("deleteFiles", "true");
      init.put("filter-name", "fileuploadfilter");
      //String[] patterns = {"* /add","* /upload","* /update"};
      m_MPFilter = new MultiPartFilter();
      wc.registerFilter(
          m_MPFilter,
          uris,
          null,
          init,
          ctx
      );
      Activator.log().debug(m_LogMarker, "activate()::registered multipart filter.");
      */
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "activate", ex);
      return false;
    }
    return true;
  }//activate

  public boolean deactivate() {
    try {
      
      //WebContainer wc = Activator.getServices().getWebContainer(ServiceMediator.NO_WAIT);
      HttpService hs = Activator.getServices().getHttpService(ServiceMediator.NO_WAIT);
      //if (wc != null) {
      if (hs != null) {
        /*
        if (m_MPFilter != null) {
          wc.unregisterFilter(m_MPFilter);
          m_MPFilter = null;
        }
        */
        if(m_MainResourceContext != null) {
          hs.unregister("/");
        }
        if (m_WSServlet != null) {
          //wc.unregisterServlet(m_WSServlet);
          hs.unregister(Activator.getServices().getWSContext());
          m_WSServlet = null;
        }
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "deactivate()", ex);
    }
    try {
      if (m_WebResourceManager != null) {
        m_WebResourceManager.deactivate();
        m_WebResourceManager = null;
      }
      SecurityService secs =
          Activator.getServices().getSecurityService(ServiceMediator.NO_WAIT);
      if (secs != null) {
        secs.invalidateAuthentication(m_ServiceAgent);
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "deactivate()", ex);
    }
    return true;
  }//deactivate

  public AgentIdentifier getAgentIdentifier(String str) {
    AgentIdentifier[] ids = null;
    AgentIdentifier aid = null;
    UserdataService ud = Activator.getServices().getUserdataService(ServiceMediator.NO_WAIT);
    try {
      ids = ud.getIdentifiers(m_ServiceAgent, str);
    } catch (SecurityException sex) {
      return null;
    }
    if (ids != null && ids.length > 0) {
      aid = ids[0];
    } else {
      aid = m_AIDCache.get(str);
    }
    return aid;
  }//getAgentIdentifier

  public String getNickname(String aidin) {
    String nick = m_NickCache.get(aidin);
    if (nick == null) {
      AgentIdentifier aid = m_AIDCache.get(aidin);
      UserdataService ud = Activator.getServices().getUserdataService(ServiceMediator.NO_WAIT);
      try {
        nick = ud.getNickname(m_ServiceAgent, aid);
        m_NickCache.put(aidin, nick);
      } catch (NoSuchElementException nsex) {
        nick = aid.getName();
      } catch (SecurityException sex) {
        nick = aid.getName();
      }
    }
    return nick;
  }//getNickname

  public DateFormatter getUTCDateFormatter() {
    return m_UTCDateFormatter;
  }//getDateFormatter

  public ServiceAgent getServiceAgent() {
    return m_ServiceAgent;
  }//getServiceAgent

  public void doMaintenance(Agent caller)
      throws SecurityException, MaintenanceException {

    //clear caches
    m_AIDCache.clear();
    m_NickCache.clear();

  }//doMaintenance

  public void update(MetaTypeDictionary mtd) {
    int aidcs = 100;
    int nickcs = 100;
    int utcdfpool = 10;
    try {
      aidcs = mtd.getInteger(Configuration.CACHE_AGENTIDENTIFIERS_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          Activator.getBundleMessages().get("WebResourceService.activation.configexception", "attribute", Configuration.CACHE_AGENTIDENTIFIERS_KEY),
          ex
      );
    }
    try {
      nickcs = mtd.getInteger(Configuration.CACHE_NICKS_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          Activator.getBundleMessages().get("WebResourceService.activation.configexception", "attribute", Configuration.CACHE_NICKS_KEY),
          ex
      );
    }
    try {
      utcdfpool = mtd.getInteger(Configuration.POOLSIZE_UTCDATEFORMATTER_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          Activator.getBundleMessages().get("WebResourceService.activation.configexception", "attribute", Configuration.POOLSIZE_UTCDATEFORMATTER_KEY),
          ex
      );
    }

    m_AIDCache.setCeiling(aidcs);
    m_NickCache.setCeiling(nickcs);
    m_UTCDateFormatter.setPoolsize(utcdfpool);
  }//update

  class WebResourceServlet
      extends HttpServlet {

    private Marker m_LogMarker = MarkerFactory.getMarker(WebResourceServlet.class.getName());

    public void service(HttpServletRequest request, HttpServletResponse response)
        throws ServletException {
      final ClassLoader cl = Thread.currentThread().getContextClassLoader();
      Thread.currentThread().setContextClassLoader(Activator.class.getClassLoader());
      try {
        WebResourceRequestImpl req =
            new WebResourceRequestImpl(WebResourceService.this, request, response);

        // Check authentication, leaves server token in response header
        try {
          if (req.isAuthenticated() && !req.authenticateTransaction()) {
            req.getHttpRequest().getSession().invalidate();
            req.failUnauthorized();
            return;
          }
        } catch (SecurityException ex) {
          req.getHttpRequest().getSession().invalidate();
          req.failUnauthorized();
          return;
        }
        // Prepare
        if (req.prepare()) {
          WebResource r = m_WebResourceManager.getWebResource(req.getResource());
          if (r == null) {
            Activator.log().debug("Resource not found: " + req.getResource());
            req.failBadRequest();
          } else {
            try {
              r.handle(req);
            } catch (WebResourceException wr) {
              Activator.log().error(m_LogMarker, "service(HttpServletRequest,HttpServletResponse)", wr);
              req.failInternalError();
            }
          }
        }

      } finally {
        Thread.currentThread().setContextClassLoader(cl);
      }
    }//service

  }//ServletImpl

  class MainResourceContext implements HttpContext {


    @Override
    public boolean handleSecurity(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {
      return true;
    }//handleSecurity

    @Override
    public URL getResource(String s) {
      if(s.endsWith("/")) {
        s += "index.html";
      }
      File f = new File (BASE_WWW_DIR + s) ;
      Activator.log().debug(m_LogMarker,"WWW::Loading" + f.toURI().toString());
      if(f.exists() && f.canRead()) {
        try {
          return f.toURI().toURL();
        } catch (MalformedURLException ex) {
          return null;
        }
      }  else {
        return null;
      }
    }//getResource

    @Override
    public String getMimeType(String s) {
      //null to indicate that the Http Service should determine the MIME type itself.
      return null;
    }//getMimeType

  }//class MainResourceContext

  public static final String UTC_STD_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS";
  private static final String BASE_WWW_DIR = System.getProperty("user.dir");

}//class WebResourceService
