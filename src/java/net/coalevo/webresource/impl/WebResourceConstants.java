/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.webresource.impl;

/**
 * This class...
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class WebResourceConstants {

  private WebResourceConstants() {
    //non-instantiateable
  }//constructor


  static final String ENCODING_HEX = "hex";
  static final String ENCODING_B64 = "b64";

  static final String WEBSESSION_ATTRIBUTE_KEY = WebSessionImpl.class.getName();

  /**
   * Custom HTTP Headers
   */

  static final String AUTHENTICITY_HEADER = "X-Coalevo-Authenticity";
  static final String CONFIDENTIALITY_HEADER = "X-Coalevo-Confidentiality";
  static final String INTEGRITY_HEADER = "X-Coalevo-Integrity";

}//class WebResourceConstants
