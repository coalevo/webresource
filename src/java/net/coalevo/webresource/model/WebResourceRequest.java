/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.webresource.model;

import net.coalevo.foundation.model.ServiceAgent;
import net.coalevo.foundation.model.UserAgent;
import net.coalevo.foundation.model.AgentIdentifier;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Map;

/**
 * Defines the interface for the resource request.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface WebResourceRequest {

  /**
   * Tests if the request method is GET.
   *
   * @return true if get, false otherwise.
   */
  public boolean isGet();

  /**
   * Tests if the request method is POST.
   *
   * @return true if get, false otherwise.
   */
  public boolean isPost();

  /**
   * Tests if the request method is PUT.
   *
   * @return true if get, false otherwise.
   */
  public boolean isPut();

  /**
   * Tests if the request method is DELETE.
   *
   * @return true if get, false otherwise.
   */
  public boolean isDelete();

  /**
   * Get the resource name.
   *
   * @return the resource <tt>String</tt>.
   */
  public String getResource();

  /**
   * Tests if this <tt>WebResourceRequest</tt> has subresources in the query.
   *
   * @return true if subresources were included, false otherwise.
   */
  public boolean hasSubResources();

  /**
   * Returns the request splitted into resources.
   *
   * @return the splitted into resources as a <tt>String[]</tt>.
   */
  public String[] getSubResources();

  /**
   * Tests if  <tt>WebResourceRequest</tt> has a query.
   *
   * @return true if there is a query in the request, false otherwise.
   */
  public boolean hasQuery();

  /**
   * Returns the query string from the request.
   *
   * @return the query string.
   */
  public String getQuery();

  /**
   * Returns the parameters of the query or the post.
   *
   * @return the parameters of the query or the post.
   */
  public Map getParameters();

  /**
   * Returns the parameter with the given key.
   *
   * @param key the key for the param.
   * @return the parameter value.
   */
  public String getParameter(String key);

  /**
   * Tests if this request has a parameter with the given key
   *
   * @param key the key of the parameter to be tested.
   * @return true if it has, false otherwise.
   */
  public boolean hasParameter(String key);

  /**
   * Sets a session attribute.
   *
   * @param key the key for the attribute.
   * @param o   the attribute value to be set.
   */
  public void setSessionAttribute(String key, Object o);

  /**
   * Returns a session attribute.
   *
   * @param key the key for the attribute.
   * @return the attribute value.
   */
  public Object getSessionAttribute(String key);

  /**
   * Removes a session attribute.
   *
   * @param key the key for the session attribute.
   */
  public void removeSessionAttribute(String key);

  /**
   * Tests if there is a given session attribute.
   *
   * @param key the key for the attribute.
   * @return true if a session attribute, false otherwise.
   */
  public boolean hasSessionAttribute(String key);

  /**
   * Returns the {@link WebSession}.
   *
   * @return the corresponding {@link WebSession}.
   */
  public WebSession getWebSession();

  /**
   * Returns the corresponding <tt>HttpServletRequest</tt>.
   *
   * @return the corresponding <tt>HttpServletRequest</tt>.
   */
  public HttpServletRequest getHttpRequest();

  /**
   * Returns the corresponding <tt>HttpServletResponse</tt>.
   *
   * @return the corresponding <tt>HttpServletResponse</tt>.
   */
  public HttpServletResponse getHttpResponse();

  /**
   * Convenience method that returns output writer for the response.
   *
   * @return a <tt>PrintWriter</tt> that corresponds to the requests output writer.
   * @throws IOException from the underlying <tt>HttpServletResponse</tt>.
   */
  public PrintWriter getOutputWriter() throws IOException;

  /**
   * Convenience method that returns output stream for the response.
   *
   * @return an <tt>OutputStream</tt> that corresponds to the requests output writer.
   * @throws IOException from the underlying <tt>HttpServletResponse</tt>.
   */
  public OutputStream getOutputStream() throws IOException;

  /**
   * Convenience method to quickly return a simple response.
   *
   * @param str the response content.
   * @throws IOException if I/O fails.
   */
  public void quickResponse(String str) throws IOException;

  /**
   * Sets the type of the corresponding response.
   *
   * @param type the mime type of the response.
   */
  public void setResponseType(String type);

  /**
   * Convenience method that sets the <tt>application/xml</tt>
   * response type.
   *
   * @see #setResponseType(String)
   */
  public void setXMLResponseType();

  /**
   * Convenience method that sets the <tt>application/json</tt>
   * response type.
   *
   * @see #setResponseType(String)
   */
  public void setJSONResponseType();

  /**
   * Convenience method that sets an <tt>image/</tt> response type.
   *
   * @param codec the codec of the image.
   */
  public void setImageResponseType(String codec);

  /**
   * Redirects to a specified URL.
   *
   * @param url the url the request should redirect to.
   * @return true if redirected, false otherwise.
   */
  public boolean redirect(String url);

  /**
   * Convenience method that will return a failure response to the
   * client.
   * The HTTP response type will correspond to an internal server error.
   *
   * @see #fail(int)
   */
  public void failInternalError();

  /**
   * Convenience method that will return a failure response to the
   * client.
   * The HTTP response type will correspond to a bad request error.
   *
   * @see #fail(int)
   */
  public void failBadRequest();

  /**
   * Convenience method that will return a failure response to the
   * client.
   * The HTTP response type will correspond to a no content response.
   *
   * @see #fail(int)
   */
  public void failNoContent();

  /**
   * Convenience method that will return a failure response to the
   * client.
   * The HTTP response type will correspond to a method not allowed
   * response.
   *
   * @see #fail(int)
   */
  public void failMethodNotAllowed();

  /**
   * Convenience method that will return a failure response to the
   * client.
   * The HTTP response type will correspond to an unauthorized response.
   *
   * @see #fail(int)
   */
  public void failUnauthorized();

  /**
   * Convenience method that will return a failure response to the
   * client.
   *
   * @param code the HTTP failure code.
   */
  public void fail(int code);

  /**
   * Convenience method that will indicate request success.
   */
  public void success();

  /**
   * Utility method that formats a UTC Date Time.
   *
   * @param d the date to be formatted.
   * @return the formatted date using an ISO STD pattern in UTC.
   */
  public String formatUTCDateTime(Date d);

  /**
   * Utility method that formats a UTC Date Time.
   *
   * @param d the date to be formatted in millis UTC.
   * @return the formatted date using an ISO STD pattern in UTC.
   */
  public String formatUTCDateTime(long d);

  /**
   * Tests if this request is authenticated.
   *
   * @return true if authenticated, false otherwise.
   */
  public boolean isAuthenticated();

  /**
   * Returns the authenticated {@link UserAgent} associated with
   * the session.
   *
   * @return a {@link UserAgent} instance.
   */
  public UserAgent getUserAgent();

  /**
   * Invalidate authentication.
   */
  public void invalidateAuthentication();

  /**
   * Public nickname resolving.
   *
   * @param aidin the agent identifier string.
   * @return the nickname associated with the aid.
   */
  public String resolveNickname(String aidin);

  /**
   * Resolves the {@link AgentIdentifier} for a given nickname.
   *
   * @param nick the nickname.
   * @return the agent identifier associated with the nickname.
   */
  public AgentIdentifier resolveAID(String nick);

  /**
   * Provides an {@link AgentIdentifier} instance for the
   * given string.
   *
   * @param aid the agent identifier string.
   * @return an {@link AgentIdentifier} instance.
   */
  public AgentIdentifier getAgentIdentifier(String aid);

  /**
   * Returns the {@link ServiceAgent} associated with
   * the web service.
   *
   * @return a {@link ServiceAgent} instance.
   */
  public ServiceAgent getServiceAgent();

}//interface WebResourceRequest
