/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.webresource.model;

import net.coalevo.text.util.DateFormatter;
import net.coalevo.webresource.impl.Activator;

import java.util.Date;
import java.util.concurrent.CountDownLatch;

/**
 * Provides an extension of {@link WebResource} that provides
 * utility methods for common tasks.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public abstract class BaseWebResource
    implements WebResource {

  private String m_Name;
  private String m_Address;
  private String m_Port;
  private String m_Context;
  private CountDownLatch m_Registered;

  public BaseWebResource(String name) {
    m_Name = name;
    m_Registered = new CountDownLatch(1);
  }//constructor

  public String getName() {
    return m_Name;
  }//getName

  public String getAddress() {
    try {
      m_Registered.await();
    } catch (InterruptedException e) {
      Activator.log().error("()", e);
    }
    return m_Address;
  }//setAddress

  public String getPort() {
    try {
      m_Registered.await();
    } catch (InterruptedException e) {
      Activator.log().error("()", e);
    }
    return m_Port;
  }//getPort

  public String getEndpoint() {
    try {
      m_Registered.await();
    } catch (InterruptedException e) {
      Activator.log().error("()", e);
    }
    return new StringBuilder()
        .append("http://")
        .append(m_Address)
        .append(":")
        .append(m_Port)
        .append(m_Context)
        .append(m_Name)
        .toString();
  }//getEndpoint

  public void setResourceBinding(String address, String port, String context) {
    m_Address = address;
    m_Port = port;
    m_Context = context;
    m_Registered.countDown();
  }//setResourceBinding

  public String formatUTCDateTime(Date d) {
    return UTC_FORMAT.format(d);
  }//formatUTCTimeDate

  public String formatUTCDateTime(long d) {
    return UTC_FORMAT.format(new Date(d));
  }//formatUTCDateTime

  public String toJSONString(String str, boolean quote) {
    //Prepare buffer
    StringBuilder sb = new StringBuilder((int) (str.length() * 1.25));
    if (quote) {
      sb.append('"');
    }
    for (int i = 0; i < str.length(); i++) {
      char c = str.charAt(i);
      switch (c) {
        case '\\':
          sb.append("\\\\");
          break;
        case '"':
          sb.append("\\\"");
          break;
        case '/':
          sb.append("\\/");
          break;
        case '\t':
        case '\b':
        case '\f':
        case '\r':
        case '\n':
          sb.append(c);

        default:
          if (c < ' ' ||
              (c >= '\u0080' && c < '\u00a0') ||
              (c >= '\u2000' && c < '\u2100')
              ) {
            sb.append("\\u");
            sb.append(String.format("%1$04x", (int) c));
          } else {
            sb.append(c);
          }
          break;
      }
    }
    if (quote) {
      sb.append('"');
    }
    return sb.toString();
  }//toJSONString

  protected static final String XML_PREFIX = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
  protected static final DateFormatter UTC_FORMAT = new DateFormatter("yyyy-MM-dd'T'HH:mm:ss.SSS", "UTC", 20);

}//class BaseResource
