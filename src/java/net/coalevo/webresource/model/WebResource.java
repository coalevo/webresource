/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.webresource.model;

/**
 * This interface defines the contract for a <tt>WebResource</tt>.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface WebResource {

  /**
   * Returns the name of this <tt>WebResource</tt>.
   * Note that the name should reflect an URI portion
   * that will form part of the service URL.
   *
   * @return the name as String.
   */
  public String getName();

  /**
   * Called by the web service to handle the request.
   * Note that <em>any</em> implementation should manage the
   * context classloader on <tt>Thread.currentThread()</tt>,
   * to ensure that there is no issue with class loading during
   * the execution.
   *
   * @param req the request encapsulated in the <tt>WebResourceRequest</tt>.
   * @throws WebResourceException if the resource fails to handle the request.
   */
  public void handle(WebResourceRequest req) throws WebResourceException;

  /**
   * Returns the address the resource is bound to respectively available at.
   * @return an address as <tt>String</tt>.
   */
  public String getAddress();

  /**
   * Returns the port the resource is bound to respectively available at.
   * @return a port number as <tt>String</tt>.
   */
  public String getPort();

  /**
   * Returns the endpoint of the resource.
   * @return
   */
  public String getEndpoint();

  /**
   * Sets the binding info of the resource (server address and port).
   * @param address the address the resource is bound to respectively available at.
   * @param port the port the resource is bound to respectively available at.
   * @param context the context of the resources in this container.
   */
  public void setResourceBinding(String address, String port, String context);

}//interface WebResource
